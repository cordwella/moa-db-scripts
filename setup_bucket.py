import boto.s3.connection
from credentials import ACCESS_KEY, SECRET_KEY, S3_HOST, BUCKET

s3 = boto.connect_s3(
    aws_access_key_id=ACCESS_KEY,
    aws_secret_access_key=SECRET_KEY,
    host=S3_HOST,
    is_secure=True,
    validate_certs=False,
    calling_format=boto.s3.connection.OrdinaryCallingFormat(),
)

MOA = s3.get_bucket(BUCKET)

