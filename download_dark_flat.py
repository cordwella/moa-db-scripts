from setup_bucket import MOA
import sys
import pyds9
import os
from astropy.io import fits
import subprocess

SAVE_DIRECTORY = "darks/"

FLAT_DB_FILENAME = "flats.dat"
DARK_DB_FILENAME = "darks.dat"


def download_darks(run_id, type_):

    if type_ == 'D':
        fmts = ["DARK/fit/dark/D{}", #-{}-{}-{}.fit",
        "DARK/fit/DARK/dark/D{}"]
    elif type_ == 'F':
        fmts = ["FLAT/fit/FLAT_ROUND/R/F{}", ]

    exposure = None

    for fmt in fmts:
        for key in MOA.list(fmt.format(run_id), ""):
            if not exposure:
                print("Exposure from {}".format(fmt))
                exposure = key.name.split("/")[-1].split('-')[1]
            save_file(key)

    return exposure

def save_file(key):
    filename = SAVE_DIRECTORY + key.name.split("/")[-1]

    directory = os.path.dirname(filename)
    if not os.path.exists(directory):
        os.makedirs(directory)

    # Skip any files that already exist
    if os.path.exists(filename):
        print(filename, "SKIP")
    else:
        print(filename)
        with open(filename, "wb") as f:
            key.get_contents_to_file(f)

def display(run_id, exposure, type_):
    viewer = pyds9.DS9()

    for l in ['a', 'b', 'c']:
        viewer.set("frame delete all")

        for frame in range(1, 11):
            filename = "{}{}{}-{}-{}-{}.fit".format(
                SAVE_DIRECTORY, type_.upper(), run_id, exposure, l, frame)
            print(filename)
            viewer.set("frame new")
            viewer.set("fits %s" % filename)
            viewer.set("zoom to fit")

        input("Continue:")

def check_good(run_id, exposure, type_):
    t = input("Is this a good set of darks?")
    if t.lower()[0] == 'y':
        data = None
        filename = None

        if type_ == 'D':
            data = "{}, {}, {}, {} \n".format(
                run_id, exposure, get_date(run_id, exposure, type_),
                get_ccd_temp(run_id, exposure, type_))
            filename = DARK_DB_FILENAME
        elif type_ == 'F':
            data = "{}, {}, {} \n".format(
                run_id, exposure, get_date(run_id, exposure, type_))
            filename = FLAT_DB_FILENAME

        with open(filename, 'a') as f:
            f.write(data)

    elif t.lower()[0] == 'n':
        t = input("Delete these darks?")
        if t.lower()[0] == 'y':
            prefix = "{}{}{}".format(SAVE_DIRECTORY, type_.upper(), run_id)
            print(prefix)
            command = "rm {}*.fit".format(prefix)
            subprocess.call("command", shell=True)


def get_ccd_temp(run_id, exposure, type_):
    t = fits.open(get_filename(run_id, exposure, type_), format='fits')
    return t[0].header["CCDTEMP"]


def get_date(run_id, exposure, type_):
    t = fits.open(get_filename(run_id, exposure, type_), format='fits')
    return t[0].header["DATE-OBS"].strip()


def get_filename(run_id, exposure, type_):
    print( "{}{}{}-{}-a-99.fit".format(
        SAVE_DIRECTORY, type_.upper(), run_id, exposure))
    return "{}{}{}-{}-a-99.fit".format(
        SAVE_DIRECTORY, type_.upper(), run_id, exposure)


if __name__ == '__main__':
    run_id = sys.argv[1]
    type_ = sys.argv[2].upper()[0] # dark or flat
    exposure = download_darks(run_id, type_)
    if exposure:
        display(run_id, exposure, type_)
        check_good(run_id, exposure, type_)
    else:
        print("No Files found")
