import boto.s3.connection
import sys
import os
from credentials import ACCESS_KEY, SECRET_KEY, S3_HOST, BUCKET

DESC = """Fake an S3 bucket as a readonly unix directory

    Implements:
    - ls: list the contents of the current directory
      usage: ls [directory]
    - cd: change current working directory
      usage: cd directory
    - cat: print to screen the contents of a file (object)
      usage: cat sourcefile
    - exit: exit this program
    - download: Download a specific file
      usage: download sourcefile [destination]
    - help: print help message
"""


s3 = boto.connect_s3(
    aws_access_key_id=ACCESS_KEY,
    aws_secret_access_key=SECRET_KEY,
    host=S3_HOST,
    is_secure=True,
    validate_certs=False,
    calling_format=boto.s3.connection.OrdinaryCallingFormat(),
)

bucket_fd = s3.get_bucket(BUCKET)
pwd = ""


def add_dirs(one, two, as_dir=True):
    if two == '.' or two == '':
        return one
    if two[0] == "/":
        return two[1:]
    if two[-1] != "/" and as_dir:
        two = two + "/"
    if two.startswith("../"):
        one = "/".join(one.split("/")[:-2])
        if one != '':
            one = one + "/"
        return add_dirs(one, two[3:])
    return one + two


def cat(parameters):
    if len(parameters) != 1:
        print("cat takes only one parameter")
    else:
        filename = pwd + parameters[0]
        key = bucket_fd.get_key(filename)
        if key:
            try:
                print(key.get_contents_as_string().decode('utf-8'))
            except UnicodeDecodeError:
                print(key.get_contents_as_string())
        else:
            print("cat: {}: No such file".format(filename))


def ls(parameters):
    directory = pwd
    if len(parameters) == 1:
        directory = add_dirs(pwd, parameters[0])

    if len(parameters) > 1:
        print("ls only takes one paramter")
    else:
        dir_length = len(directory)
        for key in bucket_fd.list(directory, "/"):
            print(key.name[dir_length:])


def cd(parameters):
    if len(parameters) != 1:
        print("cd takes only one parameter")
    else:
        global pwd
        pwd = add_dirs(pwd, parameters[0])


def download(parameters):
    # Source Final
    if len(parameters) == 1:
        source = add_dirs(pwd, parameters[0], False)
        destination = parameters[0]
    elif len(parameters) == 2:
        source = add_dirs(pwd, parameters[0], False)
        destination = parameters[1]
    else:
        print("Download only takes two parameters,",
              "source and destination (optional)")
        print("Destination is with respect to the directory",
              "running this script on the accessing computer")

    directory = os.path.dirname(destination)
    if directory and not os.path.exists(directory):
        os.makedirs(directory)

    f = open(destination, "wb")

    key = bucket_fd.get_key(source)
    if key:
        key.get_contents_to_file(f)
    else:
        print("cat: {}: No such file".format(source))
    f.close()


def main():
    while True:
        commands = input("[{}]$ ".format("~/" + pwd)).split(" ")
        command = commands[0]
        parameters = commands[1:]

        if command == "ls":
            ls(parameters)
        elif command == "cd":
            cd(parameters)
        elif command == "cat":
            cat(parameters)
        elif command == "download":
            download(parameters)
        elif command == "help":
            print(DESC)
        elif command == "exit":
            sys.exit()


if __name__ == "__main__":
    main()
