# Amelia MOA Database Scripts


The following are a few scripts developed to help access the moa database,
although they could be used for any s3 bucket realistically.

The files require a credentials.py file to be written to function correctly.
The format for this file is:

```
ACCESS_KEY = ''
SECRET_KEY = ''
BUCKET = 'moa'

at_uoa_campus = False

if at_uoa_campus:
    S3_HOST = 'object.auckland.ac.nz'
else:
    S3_HOST = 'objectext.auckland.ac.nz'
```

Requires python3, and for boto to be installed (``` pip install boto ```).

## fakedir.py

Connects to the S3 Bucket and gets it to pretend to be a read only unix directory.
```
Implements:
- ls: list the contents of the current directory
  usage: ls [directory]
- cd: change current working directory
  usage: cd directory
- cat: print to screen the contents of a file (object)
  usage: cat sourcefile
- exit: exit this program
- download: Download a specific file
  usage: download sourcefile [destination]
- help: print help message
```

## downloadlist.py

Download a group of files from the database.

## download_dark_flat.py

Download a set of darks or flats with a certain run number, check them and then
if they are good write their details to darks.dat or flats.dat.

This requires ds9 to be running as it will display the flats or the dark for checking
there.

usage
```
python download_dark_flat.py [run_id] [f or d]
```

### Contact
Contact Amelia Cordwell <acor102@aucklanduni.ac.nz> or at <cordwellamelia@gmail.com>
for help if nessacary.
