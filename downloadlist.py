import boto.s3.connection
import os
from credentials import ACCESS_KEY, SECRET_KEY, S3_HOST, BUCKET

s3 = boto.connect_s3(
    aws_access_key_id=ACCESS_KEY,
    aws_secret_access_key=SECRET_KEY,
    host=S3_HOST,
    is_secure=True,
    validate_certs=False,
    calling_format=boto.s3.connection.OrdinaryCallingFormat(),
)

bucket_fd = s3.get_bucket(BUCKET)
directory = input("Source Directory:")
dir_length = len(directory)
save_directory = input("Save Directory:")
recursive = "" if input("Recursive:")[0].lower() == 'y' else "/"
flatten = False

for key in bucket_fd.list(directory, recursive):
    filename = save_directory + key.name[dir_length:]
    if flatten:
        filename = save_directory + key.name.split("/")[-1]

    directory = os.path.dirname(filename)
    if not os.path.exists(directory):
        os.makedirs(directory)

    # Skip any files that already exist
    if os.path.exists(filename):
        print(filename, "SKIP")
    else:
        print(filename)
        with open(filename, "wb") as f:
            key.get_contents_to_file(f)
